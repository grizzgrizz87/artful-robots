# [B18][Patch] Artful Robots
Gone are the days where your builder bots destroy furniture in a fit of binary rage upon realization that they do not have a name!

> Updated for B18
* A17 files still available. Renamed

> Fixes issues with unnamed pawns creating art, especially on furniture items.
* Named Pawns will still create art with a proper Author
* Unnamed Pawns such as Robots (or animals, if such a mod is created) will create art with the Author set to "Unknown"

----

### HugsLib required.

----

### Technical details:
When a pawn (such as a robot) creates an item which contains art (such as a dining chair) the base game passes through a function which attempts to apply logic to the pawn's name. This patch has tweaked that function slightly so that if the Pawn's name is null (I mean come on, even my roomba is named "Gadget", you heartless savage!), it stops the function without causing an Object reference error.
