﻿/*
 * Created by SharpDevelop.
 * User: Joshua
 * Date: 8/11/2017
 * Time: 6:53 PM
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using RimWorld;
using RimWorld.Planet;
using Verse;
using Harmony;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Verse.AI;
using Verse.AI.Group;
using HugsLibChecker;
using HugsLib;

namespace artfuRobots {
	/// <summary>
	/// Override Rimworld Art functionality to allow Robots to create artful furniture.
	/// </summary>
	/// 
		
	[HarmonyPatch(typeof(CompArt), "JustCreatedBy", new[]{typeof(Pawn)})]
	class Patch : ModBase {
		public override string ModIdentifier
		{
			get
			{
				return "artfulRobots";
			}
		}
		
		public static void log(string message) {
			Log.Message(message);
		}
		
		
		[HarmonyPrefix]
		public static bool checkName(ref Pawn pawn) {
			if (pawn.Name == null) {
				log("Artful Robots caught and released");
				Traverse.Create<CompArt>().Property("authorNameInt").SetValue("Probably a Robot");
				return false;
			} else {
				return true;
			}
			
		}
	}
}